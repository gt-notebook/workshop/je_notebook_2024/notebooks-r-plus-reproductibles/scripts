# Des Notebooks R plus reproductibles

*More reproducible R notebooks* is a workshop given at 
[2024 seminar](https://gt-notebook.gitpages.huma-num.fr/site_quarto/seminar/20241112_JE_Notebook.html) 
of the French [notebook workgroup](https://gt-notebook.gitpages.huma-num.fr/site_quarto/)

# Goal

The aim of this workshop is to provide some tools that can be used in R notebooks 
to improve reproducibility.
This repository provide several materials to demonstrate usage of tools like 
[Quarto](https://quarto.org/), [{{targets}}](https://books.ropensci.org/targets/) and 
[{{renv}}](https://rstudio.github.io/renv/).
 
# Materials

- [Collaboratives notes (in French)](https://pinkmypad.net/hackmd/rkPPxOPZyg)
- [Support ](https://gt-notebook.gitpages.huma-num.fr/workshop/je_notebook_2024/notebooks-r-plus-reproductibles/support/)
- [Scripts (this repo)](https://gitlab.huma-num.fr/gt-notebook/workshop/je_notebook_2024/notebooks-r-plus-reproductibles/scripts)
  1. [R script](https://gitlab.huma-num.fr/gt-notebook/workshop/je_notebook_2024/notebooks-r-plus-reproductibles/scripts/-/blob/main/2019-tmja.R?ref_type=heads)
  2. [Simple Quarto notebook](./2019-tmja.html)
  3. [Quarto notebook using *targets*](./2019-tmja-targets.html)

# Data
## TMJA-2019
### Files 

- tmja2019-shp.zip

### Data source

- producer: French Ministère de la Transition Ecologique
- link: https://www.data.gouv.fr/fr/datasets/trafic-moyen-journalier-annuel-sur-le-reseau-routier-national/
- shapefile stable url : https://www.data.gouv.fr/fr/datasets/r/bf95beb9-08e8-4bd1-a734-3d9a66b2caff

### Data description

The average annual daily traffic (AADT) for a road section is obtained by calculating the annual average number of vehicles on the section, in all directions, over the course of a day.
Delayed-time traffic data are generally accompanied by an estimate (in percentage terms) of the number of heavy goods vehicles making up the traffic.

The road sections are defined by :

- its name (road)
- its length
- its start (D) and end (F) coordinates expressed in coordinates (X, Y, Z) or by a road reference system consisting of 4 attributes:
  - pr = road reference point
  - depPr = department where PR is located
  - concessionPr = indicates whether the PR is on a concession section (C) or not (N)
  - abs = abscissa or distance (in meters) separating the point from the PR to which it is attached

Other attributes complete these location attributes:

- anneeMesureTrafic = year in which measurements were taken
- TypeComptageTrafic = indicates whether vehicle counts were taken using a system permanently installed on the road, or whether they were taken using a temporary counting system.
- TMJA = average annual daily traffic
- RatioPL = percentage of heavy goods vehicles making up the AADT 

## GISCO data

To provide geographical context to the map, the script also download data
provided by the European Commission GISCO service. 

It is France's level 2 NUTS and the european countries shape.

## Requirements

1. Software to be installed first

- [Quarto](https://quarto.org/docs/get-started/)
- [R](https://cloud.r-project.org/)
- [Git](https://git-scm.com/)

We strongly recommend using [Rstudio](https://posit.co/download/rstudio-desktop/) as an IDE, as it integrates Quarto document editing and visualization features.  

2. Clone the repo

```bash
git clone https://gitlab.huma-num.fr/gt-notebook/workshop/je_notebook_2024/notebooks-r-plus-reproductibles/scripts.git
```

Using Rstudio, open the file `Notebooks-r-plus-reproducible-script.Rproj` 
then execute the following commands in the console:

```R
install.packages("renv")
```

```R
renv::restore()
```


# Authors

- Nicolas Roelandt (Univ. Eiffel)

# Licence

All material are under [![licensebuttons by-nc](https://licensebuttons.net/l/by-nc/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc/4.0) licence.