# Nicolas Roelandt (univ. Eiffel) 2024
# EUPL-1.2

library(targets)
library(tarchetypes)
tar_option_set(packages = c("here", "dplyr", "sf", "stringr"))

source("R/functions.R")

tar_plan(
  # URL vers les données
  data_url = "https://static.data.gouv.fr/resources/trafic-moyen-journalier-annuel-sur-le-reseau-routier-national/20211222-170254/tmja2019-shp.zip",
  # Téléchargement des données
  raw_data = get_raw_data(data_url),
  eu = get_europe_countries(),
  fra = get_france_regions(),
  classified_roads = road_classification(raw_data),
  road_type_table = road_agregation(classified_roads),
  pl_data = data_correction(classified_roads),
  donnees_carto = data_preparation(pl_data)
)